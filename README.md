# Onboarding - Govern

The Govern stage uses the same [onboarding template](https://gitlab.com/gitlab-org/secure/onboarding/-/blob/master/.gitlab/issue_templates/Technical_Onboarding.md) as the Secure stage.

[Create onboarding issue](https://gitlab.com/gitlab-org/secure/onboarding/-/issues/new?issuable_template=Technical_Onboarding)
